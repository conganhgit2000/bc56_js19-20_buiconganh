var arrNhanVien = [];
document.querySelector("#btnThemNV").onclick = function () {
  var nhanVienNew = new NhanVien();
  nhanVienNew.taiKhoan = document.querySelector("#tknv").value;
  nhanVienNew.hoVaTen = document.querySelector("#name").value;
  nhanVienNew.email = document.querySelector("#email").value;
  nhanVienNew.matKhau = document.querySelector("#password").value;
  nhanVienNew.ngayThang = document.querySelector("#datepicker").value;
  nhanVienNew.luongCoBan = document.querySelector("#luongCB").value;
  nhanVienNew.chucVu = document.querySelector("#chucvu").value;
  nhanVienNew.gioLam = document.querySelector("#gioLam").value;

  //tính tổng lương của nhân viên dựa theo chức vụ
  var tongLuong = "";
  if (nhanVienNew.chucVu === "Sếp") {
    tongLuong = nhanVienNew.luongCoBan * 3;
  } else if (nhanVienNew.chucVu === "Trưởng phòng") {
    tongLuong = nhanVienNew.luongCoBan * 2;
  } else {
    tongLuong = nhanVienNew.luongCoBan * 1;
  }
  nhanVienNew.tongLuong = tongLuong.toLocaleString();

  //kiểm tra xếp loại của nhân viên
  var xepLoai = "";
  if (nhanVienNew.gioLam >= 192) {
    xepLoai = "Xuất sắc";
  } else if (nhanVienNew.gioLam >= 176) {
    xepLoai = "Giỏi";
  } else if (nhanVienNew.gioLam >= 160) {
    xepLoai = "Khá";
  } else {
    xepLoai = "Trung Bình";
  }
  nhanVienNew.xepLoai = xepLoai;

  //kiểm tra không được để trống
  var valid =
    validation.kiemTraRong(nhanVienNew.taiKhoan, "taiKhoan") &
    validation.kiemTraRong(nhanVienNew.hoVaTen, "hoVaTen") &
    validation.kiemTraRong(nhanVienNew.email, "email") &
    validation.kiemTraRong(nhanVienNew.matKhau, "matKhau") &
    validation.kiemTraRong(nhanVienNew.ngayThang, "ngayThang") &
    validation.kiemTraRong(nhanVienNew.luongCoBan, "luongCoBan") &
    validation.kiemTraRong(nhanVienNew.chucVu, "chucVu") &
    validation.kiemTraRong(nhanVienNew.gioLam, "gioLam");
  //kiểm tra độ dài
  valid =
    valid &
    validation.kiemTraDoDai(nhanVienNew.taiKhoan, "taiKhoan", 4, 6) &
    validation.kiemTraDoDai(nhanVienNew.matKhau, "matKhau", 6, 10);
  //kiểm tra ký tự
  valid = valid & validation.kiemTraTatCaKyTu(nhanVienNew.hoVaTen, "hoVaTen");
  //kiểm tra email
  valid = valid & validation.kiemTraEmail(nhanVienNew.email, "email");
  //kiểm tra giá trị
  valid =
    valid &
    validation.kiemTraGiaTri(
      nhanVienNew.luongCoBan,
      "luongCoBan",
      1000000,
      20000000
    ) &
    validation.kiemTraGiaTri(nhanVienNew.gioLam, "gioLam", 80, 200);
  //kiểm tra chức vụ
  valid = valid & validation.kiemTraChucVu(nhanVienNew.chucVu, "chucVu");
  //kiểm tra mật khẩu
  valid = valid & validation.kiemTraPassword(nhanVienNew.matKhau, "matKhau");

  if (!valid) {
    return;
  }
  arrNhanVien.push(nhanVienNew);

  renderTableNhanVien(arrNhanVien);
  saveStroage();

  document.querySelector("#btnCapNhat").disabled = false;
};

document.querySelector("#btnThem").onclick = function () {
  document.querySelector("#btnCapNhat").disabled = true;
};

document.querySelector("#btnDong").onclick = function () {
  document.querySelector("#btnCapNhat").disabled = false;
};

/**
 *
 * @param {*} arrNV là mảng chứa các object nhanVien : arrNV = [{taiKhoan:1,...},{taiKhoan:2,...},...]
 */

function renderTableNhanVien(arrNV) {
  var outputHTML = "";
  for (var index = 0; index < arrNV.length; index++) {
    var nhanVien = arrNV[index];
    outputHTML += `
        <tr>
            <td>${nhanVien.taiKhoan}</td>
            <td>${nhanVien.hoVaTen}</td>
            <td>${nhanVien.email}</td>
            <td>${nhanVien.ngayThang}</td>
            <td>${nhanVien.chucVu}</td>
            <td>${nhanVien.tongLuong}</td>
            <td>${nhanVien.xepLoai}</td>
            <td>
              <button class="btn btn-danger" onclick="xoaNhanVien(${index})">Xóa</button>
              <button class="btn btn-success" onclick="suaNhanVien(${index})">Sửa</button>
            </td>
        </tr>
    `;
  }
  document.querySelector("#tableDanhSach").innerHTML = outputHTML;
}

//tính năng xóa thông tin nv
function xoaNhanVien(indexDel) {
  arrNhanVien.splice(indexDel, 1);

  //Sau khi xoá thì tạo lại table
  renderTableNhanVien(arrNhanVien);
  saveStroage();
}

//sửa thông tin nhân viên
function suaNhanVien(indexEdit) {
  var sinhVienSua = arrNhanVien[indexEdit];
  document.querySelector("#tknv").value = sinhVienSua.taiKhoan;
  document.querySelector("#name").value = sinhVienSua.hoVaTen;
  document.querySelector("#email").value = sinhVienSua.email;
  document.querySelector("#password").value = sinhVienSua.matKhau;
  document.querySelector("#datepicker").value = sinhVienSua.ngayThang;
  document.querySelector("#luongCB").value = sinhVienSua.luongCoBan;
  document.querySelector("#chucvu").value = sinhVienSua.chucVu;
  document.querySelector("#gioLam").value = sinhVienSua.gioLam;

  document.querySelector("#btnThemNV").disabled = true;
  document.querySelector("#tknv").disabled = true;
  document.querySelector("#btnDong").disabled = true;

  //hiện input
  var showInput = document.querySelector("#myModal");
  showInput.setAttribute("style", "display: block; padding-right: 17px;");
  showInput.classList.add("show");

  var showInputb = document.querySelector("body");
  showInputb.setAttribute("style", "padding-right: 17px;");
  showInputb.classList.add("modal-open");
}

//lưu thông tin vào localstroage
function saveStroage() {
  var sArrNhanVien = JSON.stringify(arrNhanVien);
  localStorage.setItem("arrNhanVien", sArrNhanVien);
}

function getStorage() {
  var strResult = localStorage.getItem("arrNhanVien");
  arrNhanVien = JSON.parse(strResult);
}

document.querySelector("#btnCapNhat").onclick = function () {
  var nhanVienUpdate = new NhanVien();
  nhanVienUpdate.taiKhoan = document.querySelector("#tknv").value;
  nhanVienUpdate.hoVaTen = document.querySelector("#name").value;
  nhanVienUpdate.email = document.querySelector("#email").value;
  nhanVienUpdate.matKhau = document.querySelector("#password").value;
  nhanVienUpdate.ngayThang = document.querySelector("#datepicker").value;
  nhanVienUpdate.luongCoBan = document.querySelector("#luongCB").value;
  nhanVienUpdate.chucVu = document.querySelector("#chucvu").value;
  nhanVienUpdate.gioLam = document.querySelector("#gioLam").value;

  var tongLuong = "";
  if (nhanVienUpdate.chucVu === "Sếp") {
    tongLuong = nhanVienUpdate.luongCoBan * 3;
  } else if (nhanVienUpdate.chucVu === "Trưởng phòng") {
    tongLuong = nhanVienUpdate.luongCoBan * 2;
  } else {
    tongLuong = nhanVienUpdate.luongCoBan * 1;
  }
  nhanVienUpdate.tongLuong = tongLuong.toLocaleString();

  //kiểm tra xếp loại của nhân viên
  var xepLoai = "";
  if (nhanVienUpdate.gioLam >= 192) {
    xepLoai = "Xuất sắc";
  } else if (nhanVienUpdate.gioLam >= 176) {
    xepLoai = "Giỏi";
  } else if (nhanVienUpdate.gioLam >= 160) {
    xepLoai = "Khá";
  } else {
    xepLoai = "Trung Bình";
  }
  nhanVienUpdate.xepLoai = xepLoai;

  for (index = 0; index < arrNhanVien.length; index++) {
    if (arrNhanVien[index].taiKhoan === nhanVienUpdate.taiKhoan) {
      arrNhanVien[index].hoVaTen = nhanVienUpdate.hoVaTen;
      arrNhanVien[index].email = nhanVienUpdate.email;
      arrNhanVien[index].matKhau = nhanVienUpdate.matKhau;
      arrNhanVien[index].ngayThang = nhanVienUpdate.ngayThang;
      arrNhanVien[index].luongCoBan = nhanVienUpdate.luongCoBan;
      arrNhanVien[index].chucVu = nhanVienUpdate.chucVu;
      arrNhanVien[index].gioLam = nhanVienUpdate.gioLam;
      arrNhanVien[index].tongLuong = nhanVienUpdate.tongLuong;
      arrNhanVien[index].xepLoai = nhanVienUpdate.xepLoai;
      break;
    }
  }

    //kiểm tra không được để trống
    var valid =
    validation.kiemTraRong(nhanVienUpdate.hoVaTen, "hoVaTen") &
    validation.kiemTraRong(nhanVienUpdate.email, "email") &
    validation.kiemTraRong(nhanVienUpdate.matKhau, "matKhau") &
    validation.kiemTraRong(nhanVienUpdate.ngayThang, "ngayThang") &
    validation.kiemTraRong(nhanVienUpdate.luongCoBan, "luongCoBan") &
    validation.kiemTraRong(nhanVienUpdate.chucVu, "chucVu") &
    validation.kiemTraRong(nhanVienUpdate.gioLam, "gioLam");
  //kiểm tra độ dài
  valid =
    valid & validation.kiemTraDoDai(nhanVienUpdate.matKhau, "matKhau", 6, 10);
  //kiểm tra ký tự
  valid =
    valid & validation.kiemTraTatCaKyTu(nhanVienUpdate.hoVaTen, "hoVaTen");
  //kiểm tra email
  valid = valid & validation.kiemTraEmail(nhanVienUpdate.email, "email");
  //kiểm tra giá trị
  valid =
    valid &
    validation.kiemTraGiaTri(
      nhanVienUpdate.luongCoBan,
      "luongCoBan",
      1000000,
      20000000
    ) &
    validation.kiemTraGiaTri(nhanVienUpdate.gioLam, "gioLam", 80, 200);
  //kiểm tra chức vụ
  valid = valid & validation.kiemTraChucVu(nhanVienUpdate.chucVu, "chucVu");
  //kiểm tra mật khẩu
  valid = valid & validation.kiemTraPassword(nhanVienUpdate.matKhau, "matKhau");

  if (!valid) {
    return;
  }

  renderTableNhanVien(arrNhanVien);
  saveStroage();

  document.querySelector("#btnThemNV").disabled = false;
  document.querySelector("#tknv").disabled = false;
  document.querySelector("#btnDong").disabled = false;

  //ẩn input
  document.querySelector("#myModal").setAttribute("style", "display: none;");
  document.querySelector("#myModal").setAttribute("aria-hidden", "true");
  document.querySelector("body").classList.remove("modal-open");
};

//tìm kiếm
document.querySelector("#searchName").oninput = function () {
  var tuKhoa = document.querySelector("#searchName").value;

  var arrSearch = [];
  for (index = 0; index < arrNhanVien.length; index++) {
    var xepLoaiNV = arrNhanVien[index].xepLoai;

    tuKhoa = stringToSlug(tuKhoa); //đổi từ chữ HOA --> thường
    xepLoaiNV = stringToSlug(xepLoaiNV); //đổi từ chữ HOA --> thường

    if (xepLoaiNV.search(tuKhoa) !== -1) {
      arrSearch.push(arrNhanVien[index]);
    }
  }

  renderTableNhanVien(arrSearch);
};

window.onload = function () {
  getStorage();
  renderTableNhanVien(arrNhanVien);
};
